package 
{
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	
	
	public class GameWorld extends World 
	{
		public static var instance:GameWorld;
		protected var ship:Ship;
		private var enemiesAmount:int = 5;
		private var enemies:Array = [];
		// Music
		[Embed(source = "../assets/Sound/8 Bit Universe - Star Wars Imperial March.mp3")]
		private const MUSIC_SFX:Class; 
		private var music:Sfx;
		// UI
		private var ui:UI;
		
		public function GameWorld() 
		{
			instance = this;
			add(new Background());
			ship = new Ship(FP.screen.width / 2, FP.screen.height / 1.2);
			add(ship);
			for (var i:int = 0; i < enemiesAmount; i++) 
			{
				var enemy:Enemy = new Enemy();
				add(enemy);
				enemies.push(enemy);
			}
			//getClass(Enemy, enemies);
			ui = new UI();
			ui.updatePlayerHealth(ship.getHealth());
			ui.updateEnemiesAmount(enemies.length);
			music = new Sfx(MUSIC_SFX);
			music.play(0.3);
		}
		
		override public function update():void
		{

			super.update();
		}
		
		public function removeEnemy(enemy:Enemy):void 
		{
			var enemyIndex:int =  enemies.indexOf(enemy);
			enemies[enemyIndex] = enemies[enemies.length - 1];
			enemies.pop();
			ui.updateEnemiesAmount(enemies.length);
			if (enemies.length == 0)
				Win();
		}
		
		public function playerHealthChanged():void 
		{
			if (ship.getHealth() >= 0)
				ui.updatePlayerHealth(ship.getHealth());
			else
				Loose();
		}
		
		private function Win():void 
		{
			trace("You Win!");
		}
		
		private function Loose():void 
		{
			trace("Game Over");
		}
		
	}
}