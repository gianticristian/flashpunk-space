package 
{
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.FP;
	
	public class UI extends Entity 
	{
		private var playerHealth:Text;
		private var enemiesAmount:Text;
		private var textOffset:int = 50;
		
		public function UI() 
		{
			playerHealth = new Text("Health: 0");
			enemiesAmount = new Text("Enemies: 0");
			SetTextUI();
			super();
		}
		
		private function SetTextUI():void 
		{
			// Player
			playerHealth.color = 0xffffff;
			playerHealth.size = 24;
			playerHealth.align = "left";
			playerHealth.x = textOffset; //playerHealth.width / 3;
			playerHealth.y = textOffset / 3;//playerHealth.height / 3;
			GameWorld.instance.addGraphic(playerHealth);
			// Enemies
			enemiesAmount.color = 0xffffff;
			enemiesAmount.size = 24;
			enemiesAmount.align = "right";
			enemiesAmount.x = FP.screen.width - enemiesAmount.width - textOffset;
			enemiesAmount.y = textOffset / 3;//enemiesAmount.height / 3;
			GameWorld.instance.addGraphic(enemiesAmount);
		}
		
		public function updatePlayerHealth(amount:int):void
		{
			playerHealth.text = "Health: " + amount;
		}
		
		public function updateEnemiesAmount(amount:int):void 
		{
			enemiesAmount.text = "Enemies: " + amount;
		}
		
		
	}

}